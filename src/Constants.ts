export class Constants {
    public static EMPTY_VALUE = "";
    public static COLUMN_PADDING_CELL_COUNT = 2;
    public static FIRST_INSERT_UID = 0;
    public static DATA_FIELD_NAME = "data";
    public static CHILD_TABLES_NAME = "kids";
    public static RECORDS_ARRAY_NAME = "records";
}
