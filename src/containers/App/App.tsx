import * as React from 'react';
import './App.css';

import {DataTable} from "../../data/data-store/DataTable";
import {DataContext} from "../../data/data-store/DataContext";
import {Table} from "../Table/Table";
import {IState} from "../../data/reducers/RootReducer";
import {connect, Dispatch} from "react-redux";
import {AnyAction} from "redux";
import {DataAccessor} from "../../data/data-access/DataAccessor";
import {setContextAC} from "../../data/reducers/action-creators/action-creators";

interface IAppProps {
    table: DataTable;
    setContext: (dc: DataContext) => void;
}

class UnconnectedApp extends React.Component<IAppProps, {}> {
    public componentDidMount() {
        const ctx = DataAccessor.getDataContext();

        this.props.setContext(ctx);
    }

    public render() {
        return (
            <Table table={this.props.table}/>
        );
    }
}



const mapStateToProps = (state: IState) => {
    return {
        table: state.data.context.table
    };
};

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) => {
    return {
        setContext: (dc: DataContext) => {
            dispatch(setContextAC(dc));
        }
    }
};

export const App = connect(mapStateToProps, mapDispatchToProps)(UnconnectedApp);
