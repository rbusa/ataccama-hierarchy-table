import 'jsdom-global/register';
import * as React from "react";
import {configure, mount, ReactWrapper} from 'enzyme';
import {IRowProps, Row} from "./Row";
import {DataContext} from "../../data/data-store/DataContext";
import {Constants} from "../../Constants";
import {Provider} from "react-redux";
import {rootReducer} from "../../data/reducers/RootReducer";
import {createStore} from "redux";
const ReactSixteenAdapter = require("enzyme-adapter-react-16");

configure({adapter: new ReactSixteenAdapter()});

describe("Row", () => {
    let props: IRowProps;
    let mountedWrapper: ReactWrapper;

    describe("Behavior without child tables", () => {
        beforeEach(() => {
            const inData = [
                {
                    data: {
                        propA: "A",
                        propB: "B"
                    },
                    kids: {}
                },
                {
                    data: {
                        propA: "A",
                        propC: "C"
                    },
                    kids: {}
                },
                {
                    data: {
                        propA: "A",
                        propB: "B",
                        propC: "C"
                    },
                    kids: {}
                }
            ];

            const dc = new DataContext();
            dc.parseData(inData);
            const table = dc.table;

            props = {
                row: table.rows[0],
                schema: table.schema,
                onDelete: jest.fn()
            };

            const component = <Row {...props}/>;

            mountedWrapper = mount(component);
        });

        it("Renders without crashing", () => {
            expect(mountedWrapper.length).toEqual(1);
        });
        it("Renders all the data specified in the \"row\" prop", () => {
            let foundA = false;
            let foundB = false;

            const foundCells = mountedWrapper.find("td");

            for (let i = 0; i < foundCells.length; ++i) {
                const cellContent = foundCells.at(i).text();

                if (cellContent === "A") {
                    foundA = true;
                }
                else if (cellContent === "B") {
                    foundB = true;
                }
            }

            expect(foundA).toEqual(true);
            expect(foundB).toEqual(true);
        });
        it("Renders empty cells for uneven data across single Table", () => {
            const columnIndex = props.schema.indexOf("propC");

            expect(
                mountedWrapper.find("td").at(columnIndex+1).text() // there's a td reserved for the expander
            ).toEqual(Constants.EMPTY_VALUE);
        });
        it("Doesn't show the expander when row has no child tables", () => {
            expect(mountedWrapper.find(".btn-rotating").length).toEqual(0);
        });
        it("Calls onDelete prop when Delete button is clicked", () => {
            mountedWrapper.find(".btn-danger").simulate("click");

            expect((props.onDelete as any).mock.calls.length).toEqual(1);
        });
    });
    describe("Behavior with child tables", () => {
        beforeEach(() => {
            const inData = [
                {
                    data: {
                        propA: "A",
                        propB: "B"
                    },
                    kids: {
                        child: {
                            records: [
                                {
                                    data: {
                                        propA: "A",
                                        propC: "C"
                                    },
                                    kids: {}
                                }
                            ]
                        }
                    }
                }
            ];

            const dc = new DataContext();
            dc.parseData(inData);
            const table = dc.table;

            props = {
                row: table.rows[0],
                schema: table.schema,
                onDelete: jest.fn()
            };

            const store = createStore(rootReducer);

            const component = (
                <Provider store={store}>
                    <Row {...props}/>
                </Provider>
            );

            mountedWrapper = mount(component);
        });

        it("Shows the expander when row has child tables", () => {
            expect(mountedWrapper.find(".btn-rotating").length).toEqual(1);
        });
        it("Doesn't show child-tables row if not expanded", () => {
            expect(mountedWrapper.find("child-tables").length).toEqual(0);
        });
        it("Shows child-tables row if is expanded and has kids", () => {
            mountedWrapper.find(".btn-rotating").simulate("click");

            expect(mountedWrapper.find(".child-tables").length).toEqual(1);
        });
        it("Expander has \"rotate\" class when expanded", () => {
            mountedWrapper.find(".btn-rotating").simulate("click");

            expect(mountedWrapper.find(".btn-rotating").hasClass("rotate")).toEqual(true);
        });
        it("Expander has no \"rotate\" class when not expanded", () => {
            expect(mountedWrapper.find(".btn-rotating").hasClass("rotate")).toEqual(false);
        });
    });
});
