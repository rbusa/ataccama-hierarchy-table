import 'jsdom-global/register';
import * as React from "react";
import {configure, mount} from 'enzyme';
import {rootReducer} from "../../data/reducers/RootReducer";
import {createStore} from "redux";
import {Provider} from "react-redux";
import {DataRow} from "../../data/data-store/DataRow";
import {DataTable} from "../../data/data-store/DataTable";
import {App} from "./App";
const ReactSixteenAdapter = require("enzyme-adapter-react-16");

configure({adapter: new ReactSixteenAdapter()});

it('Renders without crashing', () => {
    const props = {
        table: new DataTable(),
        setContext: (dr: DataRow) => {}
    };

    const store = createStore(rootReducer);

    const component = (
        <Provider store={store}>
            <App {...props}/>
        </Provider>
    );

    const mountedWrapper = mount(component);

    expect(mountedWrapper.length).toEqual(1);
});
