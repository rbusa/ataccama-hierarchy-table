# Presentation Layer
Bootstrap 4 is used as CSS framework.

The React app uses the *Presentational and Container Components* design pattern.

# Component diagram
The rest of this document explains the contents of this diagram.

![data layer structure](img/presentation-layer.png)

# Component structure

Presentation layer consists of React components as described below:

## App
Root component of the app.
It fetches the data from Data Layer and contains root `Table` component.

## Table
`Table` renders a single `DataTable` object.

If the table doesn't contain any rows, then it will render a message informing the user about lack of data.

`Table` consists of a single `TableHeader` component, one to many `Row` components and zero to one `TableCaption` component.

## Row
`Row` renders a single `DataRow` object.
`Row` consists of `Cell` components.

It holds a state whether or not it is expanded to show `kids` structure if present.

## Cell
`Cell` represents a primitive data value.

## HeaderCell
`HeaderCell` represents a cell of `TableHeader`, typically containing column name.

## TableHeader
`TableHeader` represents a table header.

`TableHeader` itself consists of `HeaderCell` components.

# Detailed documentation
Detailed documentation of data structure behavior is present in JSDoc comments in the source files.