import {DataContext} from "../DataContext";

describe("DataRow", () => {
    it("hasKids returns true if DataRow has kids", () => {
        const data = [
            {
                data: {
                    name: "Hello"
                },
                kids: {
                    world: {
                        records: [
                            {
                                data: {
                                    name: "World"
                                },
                                kids: {}
                            }
                        ]
                    }
                }
            }
        ];

        const dc = new DataContext();
        dc.parseData(data);

        expect(dc.table.rows[0].hasKids()).toEqual(true);
    });
    it("hasKids returns false if DataRow has no kids", () => {
        const data = [
            {
                data: {
                    name: "Hello"
                },
                kids: {}
            }
        ];

        const dc = new DataContext();
        dc.parseData(data);

        expect(dc.table.rows[0].hasKids()).toEqual(false);
    });
});
