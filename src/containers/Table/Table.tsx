import * as React from 'react';
import './Table.scss';
import {Row} from "../../components/Row/Row";
import {DataTable} from "../../data/data-store/DataTable";
import {DataRow} from "../../data/data-store/DataRow";
import {TableHeader} from "../../components/TableHeader/TableHeader";
import {TableCaption} from "../../components/TableCaption/TableCaption";
import {connect, Dispatch} from "react-redux";
import {AnyAction} from "redux";
import {deleteRowAC} from "../../data/reducers/action-creators/action-creators";

export interface ITableProps {
    table: DataTable;
    deleteRow: (dr: DataRow) => void;
}

class UnconnectedTable extends React.Component<ITableProps, {}> {
    public render() {
        const rows = this.props.table.rows.map(dataRow => {
            return <Row key={dataRow.uid} row={dataRow} schema={this.props.table.schema} onDelete={this.handleDelete}/>
        });

        return (
            <React.Fragment>
                {
                    this.props.table.rows.length > 0 ?
                    <table className="table table-hover">
                        {
                            (this.props.table.caption !== undefined) &&
                            <TableCaption text={this.props.table.caption}/>
                        }
                        <TableHeader schema={this.props.table.schema}/>
                        <tbody>
                        {rows}
                        </tbody>
                    </table>
                    :
                    <p className="h3 text-center text-muted m-5">No data</p>
                }
            </React.Fragment>
        );
    }

    private handleDelete = (tr: DataRow) => {
        this.props.deleteRow(tr);
    };
}

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) => {
    return {
        deleteRow: (dr: DataRow) => {
            dispatch(deleteRowAC(dr));
        }
    }
};

export const Table = connect(null, mapDispatchToProps)(UnconnectedTable);
