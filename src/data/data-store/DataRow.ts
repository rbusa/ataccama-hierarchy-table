import {DataContext} from "./DataContext";
import {DataTable} from "./DataTable";
import {DataContextError} from "./errors/DataContextError";
import {Constants} from "../../Constants";

export class DataRow {
    public uid: number;
    public data: any;
    public kids: Map<string, DataTable>;

    public constructor() {
        this.kids = new Map<string, DataTable>();
    }

    /**
     * Populates this object with parsed data
     * @param data
     * @param {DataContext} ctx
     */
    public parseFromData(data: any, ctx: DataContext): void {
        if (data.hasOwnProperty(Constants.DATA_FIELD_NAME)
        && data.hasOwnProperty(Constants.CHILD_TABLES_NAME)) { // it's a DataRow!
            this.uid = ctx.getNewUID();
            this.data = data[Constants.DATA_FIELD_NAME]; // :)

            for (const kidName in data[Constants.CHILD_TABLES_NAME]) {
                if (data[Constants.CHILD_TABLES_NAME].hasOwnProperty(kidName)) {
                    const kidData = data[Constants.CHILD_TABLES_NAME][kidName];

                    if (kidData.hasOwnProperty(Constants.RECORDS_ARRAY_NAME)) {
                        const newTable = new DataTable();
                        newTable.parseFromData(kidData.records, ctx);

                        newTable.caption = kidName;

                        this.kids.set(kidName, newTable);
                    }
                    else {
                        throw new DataContextError("Supplied data has incorrect 'kids' structure.");
                    }
                }
            }
        }
        else {
            throw new DataContextError("Supplied data is not a row.");
        }
    }
    public hasKids(): boolean {
        return (this.kids.size > 0);
    }
}
