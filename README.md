# Ataccama Hierarchy Table
This project is made for Ataccama interview to assess my software engineering skills.

## Building and running
1. `npm install`
2. `npm start`

## Testing
1. (`npm install`)
2. `npm test`

## More docs
More detailed docs are in the `docs` directory of this project.
