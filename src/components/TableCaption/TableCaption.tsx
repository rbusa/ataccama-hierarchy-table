import "./TableCaption.css";
import * as React from "react";

interface ITableCaptionProps {
    text: string;
};

export const TableCaption: React.SFC<ITableCaptionProps> = (props: ITableCaptionProps) => {
    return (
        <caption className="p-3 m-0 h3 tablecaption">
            {props.text}
        </caption>
    );
};
