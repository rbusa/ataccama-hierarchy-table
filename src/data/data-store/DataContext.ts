import {DataTable} from "./DataTable";
import {Constants} from "../../Constants";

export class DataContext {
    public table: DataTable;
    private lastInsertUID: number;

    public constructor() {
        this.table = new DataTable();
        this.lastInsertUID = Constants.FIRST_INSERT_UID;
    }
    public getNewUID(): number {
        return (++this.lastInsertUID);
    }
    public deleteByUID(uid: number): boolean {
        return this.table.deleteRecursively(uid);
    }
    public parseData(data: any): void {
        this.table.parseFromData(data, this);
    }
}
