import * as React from "react";
import {ICellProps} from "../Cell/Cell";

export const HeaderCell: React.SFC<ICellProps> = (props: ICellProps) => {
    return (
        <th>{props.value}</th>
    );
};
