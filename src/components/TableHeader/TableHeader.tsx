import * as React from "react";
import {HeaderCell} from "../HeaderCell/HeaderCell";

interface ITableHeaderProps {
    schema: string[];
};

export const TableHeader: React.SFC<ITableHeaderProps> = (props: ITableHeaderProps) => {
    const headerCells = props.schema.map((cellData, index) => {
        return <HeaderCell key={index} value={cellData}/>
    });

    return (
        <thead className="thead-light">
            <tr>
                <th/>
                {headerCells}
                <th/>
            </tr>
        </thead>
    );
};
