import * as React from "react";

export interface ICellProps {
    value: string;
}

export const Cell: React.SFC<ICellProps> = (props: ICellProps) => {
    return (
        <td>{props.value}</td>
    );
};
