import {DataContext} from "../data-store/DataContext";
import {sampleData} from "../data-store/data-source/sample-data";

export class DataAccessor {
    public static getDataContext(): DataContext {
        const dc = new DataContext();
        dc.parseData(sampleData);

        return dc;
    }
}
