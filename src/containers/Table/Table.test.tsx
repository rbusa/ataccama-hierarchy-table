import 'jsdom-global/register';
import * as React from "react";
import {configure, mount, ReactWrapper} from 'enzyme';
import {ITableProps, Table} from "./Table";
import {DataContext} from "../../data/data-store/DataContext";
import {rootReducer} from "../../data/reducers/RootReducer";
import {createStore} from "redux";
import {Provider} from "react-redux";
import {DataRow} from "../../data/data-store/DataRow";
const ReactSixteenAdapter = require("enzyme-adapter-react-16");

configure({adapter: new ReactSixteenAdapter()});

describe("Table", () => {
    let props: ITableProps;
    let mountedWrapper: ReactWrapper;

    describe("Has rows", () => {
        beforeEach(() => {
            const inData = [
                {
                    data: { // UID = 1
                        propA: "A",
                        propB: "B"
                    },
                    kids: {}
                },
                {
                    data: { // UID = 2
                        propA: "A1",
                        propB: "B1"
                    },
                    kids: {}
                }
            ];

            const dc = new DataContext();
            dc.parseData(inData);
            const t = dc.table;

            props = {
                table: t,
                deleteRow: (dr: DataRow) => {}
            };

            const store = createStore(rootReducer);

            const component = (
                <Provider store={store}>
                    <Table {...props}/>
                </Provider>
            );

            mountedWrapper = mount(component);
        });

        it("Renders without crashing", () => {
            expect(mountedWrapper.length).toEqual(1);
        });
        it("Renders all the rows specified by \"table\" prop", () => {
            expect(mountedWrapper.find("tbody tr").length).toBe(2);
        });
    });

    describe("Has no rows", () => {
        it("Renders \"No data\" if supplied table has no rows", () => {
            const dc = new DataContext();
            dc.parseData([]);
            const t = dc.table;

            props = {
                table: t,
                deleteRow: (dr: DataRow) => {}
            };

            const store = createStore(rootReducer);

            const component = (
                <Provider store={store}>
                    <Table {...props}/>
                </Provider>
            );

            mountedWrapper = mount(component);

            expect(mountedWrapper.find(".text-muted").length).toEqual(1);
        });
    });
});
