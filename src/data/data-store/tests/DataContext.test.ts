import {DataContext} from "../DataContext";

describe("DataContext", () => {
    it("Parses data correctly", () => {
        const data = [
            {
                data: {
                    name: "Hello"
                },
                kids: {
                    world: {
                        records: [
                            {
                                data: {
                                    name: "World"
                                },
                                kids: {}
                            }
                        ]
                    }
                }
            },
            {
                data: {
                    name: "JavaScript"
                },
                kids: {}
            }
        ];

        const dc = new DataContext();
        dc.parseData(data);

        expect(dc.table.rows[0].data.name).toEqual("Hello");
        expect(dc.table.rows[0].kids.size).toEqual(1);
        expect(dc.table.rows[0].kids.get("world")!.rows[0].data.name).toEqual("World");

        expect(dc.table.rows[1].data.name).toEqual("JavaScript");
        expect(dc.table.rows[1].kids.size).toEqual(0);
    });
    it("Throws an exception if parser gets incorrect data structure (1 - mispelled properties)", () => {
        const data = [
            {
                tada: {
                    name: "Hello"
                },
                kids: {}
            }
        ];

        expect(() => {
            const dc = new DataContext();
            dc.parseData(data);
        }).toThrow("Supplied data is not a row.");
    });
    it("Throws an exception if parser gets incorrect data structure (2 - missing properties)", () => {
        const data = [
            {
                data: {
                    name: "Hello"
                }
            }
        ];

        expect(() => {
            const dc = new DataContext();
            dc.parseData(data);
        }).toThrow("Supplied data is not a row.");
    });
    it("Throws an exception if 'kids' object has incorrect structure", () => {
        const data = [
            {
                data: {
                    name: "Hello"
                },
                kids: {
                    records: [
                        {
                            data: {
                                name: "World"
                            },
                            kids: {}
                        }
                    ]
                }
            },
            {
                data: {
                    name: "JavaScript"
                },
                kids: {}
            }
        ];

        expect(() => {
            const dc = new DataContext();
            dc.parseData(data);
        }).toThrow("Supplied data has incorrect 'kids' structure.");
    });
});
