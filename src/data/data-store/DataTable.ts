import {DataContext} from "./DataContext";
import {DataRow} from "./DataRow";
import {DataContextError} from "./errors/DataContextError";

export class DataTable {
    public caption: string;
    public schema: string[];
    public rows: DataRow[];

    constructor() {
        this.rows = [];
        this.schema = [];
    }

    /**
     * Populates this object with parsed data
     * @param data
     * @param {DataContext} ctx
     */
    public parseFromData(data: any, ctx: DataContext): void {
        if (Array.isArray(data)) {
            for (const row of data) {
                const newRow = new DataRow();
                newRow.parseFromData(row, ctx);

                this.checkSchema(newRow.data);

                this.rows.push(newRow);
            }
        }
        else {
            throw new DataContextError("Supplied data is not an array.");
        }
    }

    /**
     * Deletes a DataRow with supplied UID within this table and child tables
     * @param {number} uid - UID of data row to be deleted
     * @return {boolean} - true if found and deleted; false otherwise
     */
    public deleteRecursively(uid: number): boolean {
        const deletedHere = this.deleteHere(uid);

        if (deletedHere) {
            return true;
        }
        else {
            for (const r of this.rows) {
                if (r.hasKids()) {
                    for (const kidTable of r.kids.values()) {
                        const deletedThere = kidTable.deleteRecursively(uid);

                        if (deletedThere) {
                            if (kidTable.rows.length === 0) {
                                r.kids.delete(kidTable.caption);
                            }

                            return deletedThere;
                        }
                    }
                }
            }

            return false;
        }
    }

    /**
     * Deletes a DataRow within this table
     * @param {number} uid - UID of data row to be deleted
     * @return {boolean} - true if found and deleted; false otherwise
     */
    private deleteHere(uid: number): boolean {
        const foundIndex = this.rows.findIndex(r => r.uid === uid);

        if (foundIndex !== -1) {
            this.rows.splice(foundIndex, 1);

            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Completes the schema with new keys of rowData
     * @param {object} rowData
     */
    private checkSchema(rowData: object): void {
        for (const dataProp of Object.keys(rowData)) {
            const foundProp = (this.schema.indexOf(dataProp) !== -1);

            if (!foundProp) {
                this.schema.push(dataProp);
            }
        }
    }
}
