import {DataReducer, IDataState} from "./DataReducer/DataReducer";
import {combineReducers} from "redux";

export interface IState {
    data: IDataState
}

export const rootReducer = combineReducers({
    data: DataReducer
});
