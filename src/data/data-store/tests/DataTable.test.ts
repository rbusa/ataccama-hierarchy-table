import {DataContext} from "../DataContext";
import {DataTable} from "../DataTable";

describe("DataTable", () => {
    const data = [
        {
            data: {
                name: "Hello" // UID = 1
            },
            kids: {
                world: {
                    records: [
                        {
                            data: {
                                name: "World" // UID = 2
                            },
                            kids: {}
                        }
                    ]
                }
            }
        },
        {
            data: {
                name: "JavaScript" // UID = 3
            },
            kids: {}
        },
        {
            data: {
                name: "NodeJS" // UID = 4
            },
            kids: {
                languages: {
                    records: [
                        {
                            data: {
                                name: "C++" // UID = 5
                            },
                            kids: {}
                        }
                    ]
                }
            }
        }
    ];

    let dt: DataTable;

    beforeEach(() => {
        const dc = new DataContext();
        dc.parseData(data);

        dt = dc.table;
    });

    it("Deletes proper element ", () => {
        dt.deleteRecursively(1);

        expect(dt.rows[0].uid).toEqual(3);
    });
    it("Deletes proper element in child table (1 - first table with child)", () => {
        dt.deleteRecursively(2);

        expect(dt.rows[0].hasKids()).toEqual(false);
    });
    it("Deletes proper element in child table (2 - nonfirst table with child)", () => {
        dt.deleteRecursively(5);

        expect(dt.rows[2].hasKids()).toEqual(false);
    });
    it("Doesn't delete anything if item could not be found", () => {
        expect(dt.deleteRecursively(1000)).toEqual(false);
        expect(dt.rows[0].uid).toEqual(1);
        expect(dt.rows[1].uid).toEqual(3);
        expect(dt.rows[0].kids.get("world")!.rows[0].uid).toEqual(2);
    });
});