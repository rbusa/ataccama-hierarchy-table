import {AnyAction, Reducer} from "redux";
import {DataContext} from "../../data-store/DataContext";
import {ReducerActions} from "../ReducerActions";
import * as _ from "lodash";

export interface IDataState {
    context: DataContext;
}

const initialState: IDataState = {
    context: new DataContext()
};

export const DataReducer: Reducer<IDataState> = (state: IDataState = initialState, action: AnyAction) => {
    switch (action.type) {
        case ReducerActions.SET_CONTEXT: {
            return {
                ...state,
                context: action.payload.context
            }
        }
        case ReducerActions.DELETE_ROW: {
            const rowUID = action.payload.uid;
            const deleted = state.context.deleteByUID(rowUID);

            if (deleted) {
                return {
                    ...state,
                    context: _.cloneDeep(state.context)
                };
            }
            else {
                throw new Error(`Row with UID ${rowUID} not found.`);
            }
        }
        default: {
            return state;
        }
    }
};
