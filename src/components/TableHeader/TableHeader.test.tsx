import 'jsdom-global/register';
import * as React from "react";
import {configure, mount, ReactWrapper} from 'enzyme';
import {TableHeader} from "./TableHeader";
import {Constants} from "../../Constants";
const ReactSixteenAdapter = require("enzyme-adapter-react-16");

configure({adapter: new ReactSixteenAdapter()});

describe("TableHeader", () => {
    const props = {
        schema: ["Hello", "World"]
    };

    let mountedWrapper: ReactWrapper;

    beforeEach(() => {
        const component = <TableHeader {...props}/>;

        mountedWrapper = mount(component);
    });

    it("Renders without crashing", () => {
        expect(mountedWrapper.length).toEqual(1);
    });
    it("Renders cells according to \"schema\" prop", () => {
        expect(mountedWrapper.find("th").length)
            .toEqual(props.schema.length+Constants.COLUMN_PADDING_CELL_COUNT);
    });
});