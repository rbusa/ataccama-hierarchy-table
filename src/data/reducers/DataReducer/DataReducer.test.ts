import {DataReducer} from "./DataReducer";
import {DataContext} from "../../data-store/DataContext";
import {DataAccessor} from "../../data-access/DataAccessor";
import {ReducerActions} from "../ReducerActions";

describe("DataReducer", () => {
    it("Should return initial state", () => {
        expect(DataReducer(<any>undefined, <any>{})).toEqual({
            context: new DataContext()
        });
    });
    it("Should handle SET_CONTEXT", () => {
        const ctx = DataAccessor.getDataContext();

        expect(
            DataReducer({
                context: new DataContext()
            }, {
                type: ReducerActions.SET_CONTEXT,
                payload: {
                    context: ctx
                }
            })
        ).toEqual({
            context: ctx
        });
    });
    it("Should handle DELETE_ROW", () => {
        const data = [
            {
                data: {
                    name: "Hello"
                },
                kids: {}
            },
            {
                data: {
                    name: "World"
                },
                kids: {}
            }
        ];

        const ctx = new DataContext();
        ctx.parseData(data);

        const newState = DataReducer({
            context: ctx
        }, {
            type: ReducerActions.DELETE_ROW,
            payload: {
                uid: 1
            }
        });

        expect(newState.context.table.rows.length).toEqual(1);
        expect(newState.context.table.rows[0].uid).toEqual(2);
    });
});
