import {DataContext} from "../../data-store/DataContext";
import {DataRow} from "../../data-store/DataRow";
import {deleteRowAC, setContextAC} from "./action-creators";
import {ReducerActions} from "../ReducerActions";

describe("Action Creators", () => {
    it("Should create right action for SET_CONTEXT", () => {
        const dc = new DataContext();

        expect(setContextAC(dc)).toEqual({
            type: ReducerActions.SET_CONTEXT,
            payload: {
                context: dc
            }
        });
    });
    it("Should create right action for DELETE_ROW", () => {
        const dr = new DataRow();
        dr.uid = 1;

        expect(deleteRowAC(dr)).toEqual({
            type: ReducerActions.DELETE_ROW,
            payload: {
                uid: 1
            }
        });
    });
});
