# Input Data Format

*Input data* is an array consisting of objects which is a *Data Table*.

## Data Table structure

```
[
    ...
]
```

Data Table consists of zero to many *Data Row*s.

## Data Row structure

```
{
    "data": ...,
    "kids": ...
}
```

Each row consists of:
- `data` (mandatory) - an object containing attributes and their primitive values
- `kids` (mandatory) - an object containing zero to one attributes

## `kids` structure

The `kids` object contains zero to many attributes.
Attribute, if present, is named as a child table.

The `kids.<attribute>` contains a `record` attribute which holds another *Data Table*

# Data Context

Data Context is a subsystem of the Data Layer which abstracts given data into an in-memory database-like structure.

![data layer structure](img/data-layer.png)

# State management
Redux is used as central state management library in order to reduce boilerplate code.

The `RootStore` uses `DataReducer` for changing the state.

## DataReducer
`DataReducer` supports two actions:
- `SET_CONTEXT` will set `context` of the store to value supplied in payload of the action.
- `DELETE_ROW` will delete a row with given UID from the `context`.

# Detailed documentation
Detailed documentation of data structure behavior is present in JSDoc comments in the source files.
