import "./Row.css";
import {DataRow} from "../../data/data-store/DataRow";
import * as React from "react";
import {Cell} from "../Cell/Cell";
import {Table} from "../../containers/Table/Table";
import {Constants} from "../../Constants";

export interface IRowProps {
    row: DataRow;
    schema: string[];
    onDelete: (dr: DataRow) => void;
}

interface IRowUIState {
    isExpanded: boolean;
}

export class Row extends React.Component<IRowProps, IRowUIState> {
    public props: IRowProps;
    public state: IRowUIState = {
        isExpanded: false
    };

    public render(): any {
        const cells = this.props.schema.map((dataKey, index) => {
            return <Cell key={index} value={this.props.row.data[dataKey] || Constants.EMPTY_VALUE}/>
        });

        let childTables = null;

        if (this.props.row.hasKids() && this.state.isExpanded) {
            const columnCount = Object.keys(this.props.row.data).length + Constants.COLUMN_PADDING_CELL_COUNT;
            
            const tableComponents = [];
            
            for (const childTable of this.props.row.kids.values()) {
                tableComponents.push(
                    <Table key={childTable.caption} table={childTable}/>
                );
            }
            
            childTables = (
                <tr className="child-tables">
                    <td colSpan={columnCount}>
                        {tableComponents}
                    </td>
                </tr>
            );
        }

        return (
            <React.Fragment>
                <tr>
                    <td className="min">
                        {
                            this.props.row.hasKids() ?
                                <button className={this.expanderClasses} onClick={this.handleExpand}>
                                    &#x25BA;
                                </button>
                                :
                                null
                        }
                    </td>
                    {cells}
                    <td className="min">
                        <button className="btn btn-sm btn-danger" onClick={this.handleDelete}>
                            Delete
                        </button>
                    </td>
                </tr>
                {childTables}
            </React.Fragment>
        );
    }

    private handleExpand = () => {
        this.setState(prevState => {
            return {
                isExpanded: !(prevState.isExpanded)
            };
        });
    };

    private handleDelete = () => {
        this.props.onDelete(this.props.row);
    };

    get expanderClasses(): string {
        const defaultClasses = "btn btn-sm btn-transparent btn-rotating";
        const expandedClass = "rotate";

        if (this.state.isExpanded) {
            return `${defaultClasses} ${expandedClass}`;
        }
        else {
            return defaultClasses;
        }
    }
}
