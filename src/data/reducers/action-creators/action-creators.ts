import {DataRow} from "../../data-store/DataRow";
import {ReducerActions} from "../ReducerActions";
import {DataContext} from "../../data-store/DataContext";

export function deleteRowAC(dr: DataRow) {
    const rowUID = dr.uid;

    return {
        type: ReducerActions.DELETE_ROW,
        payload: {
            uid: rowUID
        }
    };
}

export function setContextAC(ctx: DataContext) {
    return {
        type: ReducerActions.SET_CONTEXT,
        payload: {
            context: ctx
        }
    };
}