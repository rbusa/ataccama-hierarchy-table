import * as React from 'react';
import * as ReactDOM from 'react-dom';
import "bootstrap/dist/css/bootstrap.css";
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import {App} from "./containers/App/App";
import {rootReducer} from "./data/reducers/RootReducer";
import {Provider} from "react-redux";
import {createStore} from "redux";

export const store = createStore(rootReducer);

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>
    ,
    document.getElementById('root') as HTMLElement
);
registerServiceWorker();
